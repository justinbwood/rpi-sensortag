# rpi-sensortag-statsd

Reader for Texas Instruments SensorTag using Bluetooth.

Supported modules include the CC2450 and CC2650.

Metrics can be exported to statsd. Additional modules can be added by implementing `IHandler`s in `sensortag_callback_interface`

## Bluetooth Access
If you do not wish to run the container with `host` networking, you may need to mount the following volumes/devices:
```
volumes:
  - /var/run/dbus:/var/run/dbus
devices:
  - /dev/bus/usb/###/###:/dev/ttyBT
```
Replace the usb path with the bus hosting the Bluetooth dongle.