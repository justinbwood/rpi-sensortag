FROM hypriot/rpi-python

RUN apt-get update && apt-get upgrade -y --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*

ADD . /build

ADD https://bootstrap.pypa.io/get-pip.py /build/get-pip.py

WORKDIR /build

RUN python get-pip.py

RUN pip install -r dependencies/pip.txt

WORKDIR /build/code
RUN pip install --editable .

ENV FLASK_APP sensortag
ENV FLASK_DEBUG true

RUN flask initdb
CMD ["flask", "run", "--host=0.0.0.0"]
