create table if not exists sensortags (
	sensorid integer primary key autoincrement,
	sensorlabel text not null,
	sensoraddr char(17) not null,
	sensorconfig integer,
	foreign key(sensorconfig) references sensorconfig(configid)
);

create table if not exists sensorconfig (
	configid integer primary key autoincrement,
	configlabel text not null,
	tempenabled boolean not null,
	tempconfig integer,
	humenabled boolean not null,
	humconfig integer,
	foreign key(tempconfig) references tempconfig(configid),
	foreign key(humconfig) references humconfig(configid)
);

create table if not exists tempconfig (
	configid integer primary key autoincrement,
	tempdataaddr text not null,
	tempnotifaddr text not null,
	tempsensoraddr text not null,
	tempperiodaddr text not null
);

create table if not exists humconfig (
	configid integer primary key autoincrement,
	humdatalength int not null,
	humdataaddr text not null,
	humreqcalib boolean not null,
	humcalibaddr text,
	humnotifaddr text not null,
	humsensoraddr not null,
	humperiodaddr text not null
);
