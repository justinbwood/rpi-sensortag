from flask import Blueprint, flash, redirect, render_template, request, session, url_for
from flask import current_app as FLASK_APP
from sensortag_model import DB, SensorTag, SensorConfig
from sensortag_forms import LoginForm, SensorTagForm, SensorConfigForm

WEB = Blueprint('sensortag_web', __name__, template_folder='templates')

@WEB.route('/sensortags', methods=['GET', 'POST'])
def show_sensortags():
    form = SensorTagForm(request.form)
    form.sensorconfig.choices = \
        [(c.configid, c.configlabel) for c in SensorConfig.query.order_by('configlabel')]
    if request.method == 'POST' and form.validate():
        sensortag = SensorTag(sensorlabel=form.sensorlabel.data,
                              sensoraddr=form.sensoraddr.data,
                              sensorconfig=form.sensorconfig.data)
        DB.session.add(sensortag)
        DB.session.commit()
        flash('New SensorTag was created')
    entries = SensorTag.query.all()
    return render_template('show_sensortags.html', entries=entries, form=form)

@WEB.route('/sensorconfigs', methods=['GET', 'POST'])
def show_sensorconfigs():
    form = SensorConfigForm(request.form)
    if request.method == 'POST' and form.validate():
        sensorconfig = SensorConfig(configlabel=form.configlabel.data,
                                    tempenabled=form.tempenabled.data,
                                    tempdataaddr=form.tempdataaddr.data,
                                    tempnotifaddr=form.tempnotifaddr.data,
                                    tempsensoraddr=form.tempsensoraddr.data,
                                    tempperiodaddr=form.tempperiodaddr.data,
                                    humenabled=form.humenabled.data,
                                    humdatalength=form.humdatalength.data,
                                    humreqcalib=form.humreqcalib.data,
                                    humcalibaddr=form.humcalibaddr.data,
                                    humdataaddr=form.humdataaddr.data,
                                    humnotifaddr=form.humnotifaddr.data,
                                    humsensoraddr=form.humsensoraddr.data,
                                    humperiodaddr=form.humperiodaddr.data)
        DB.session.add(sensorconfig)
        DB.session.commit()
        flash('New SensorTag Config was created')
    if request.method == 'POST' and not form.validate():
        print "Form didn't validate?"
    entries = SensorConfig.query.all()
    return render_template('show_sensorconfigs.html', entries=entries, form=form)

@WEB.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != FLASK_APP.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != FLASK_APP.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in successfully')
            return redirect(url_for('sensortag_web.show_sensortags'))
    return render_template('login.html', error=error)

@WEB.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('sensortag_web.show_sensortags'))
