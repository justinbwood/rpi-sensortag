"""
Calculations for sensors included in old (e.g. CC2450) and
    new (e.g. CC2650) Texas Instruments SensorTags

    Supported by CC2450:
        IR Temperature        tmp006
        Humidity               sht21
        Accelerometer          kxtj9
        Gyroscope            imu3000
        Magnetometer         mag3110
        Barometric Pressure    t5400
    Supported by CC2650:
        IR Temperature        tmp007
        Humidity             hdc1000
        Accelerometer        mpu9250
        Gyroscope            mpu9250
        Magnetometer         mpu9250
        Barometric Pressure   bmp280
        Optical              opt3001
"""

from itertools import izip

def tosignedbyte(val):
    """
    Convert one unsigned byte to signed int
    """
    return float(val - 0x100) if val > 0x7f else float(val)

def tosigned(val):
    """
    Convert two unsigned bytes to signed int
    """
    return float(val - 0x10000) if val > 0x7fff else float(val)

def calc_temp_tmp006(obj_temp, amb_temp):
    """
    IR Temperature for Texas Instruments TMP006
    """
    calib = {
        's0': 6.4E-14,
        'a1': 1.75E-3,
        'a2': -1.678E-5,
        'b0': -2.94E-5,
        'b1': -5.7E-7,
        'b2': 4.63E-9,
        'c2': 13.4,
        'tref': 298.15
    }
    obj_temp = tosigned(obj_temp)
    obj_scale = obj_temp * 0.00000015625
    amb_temp = float(tosigned(amb_temp)) / 128.0
    die_kelv = amb_temp + 273.15
    s_obj_temp = calib['s0'] * (1 + calib['a1'] * (die_kelv - calib['tref']) \
                 + calib['a2'] * pow((die_kelv - calib['tref']), 2))
    v_obj_temp = calib['b0'] + calib['b1'] * (die_kelv - calib['tref']) \
                 + calib['b2'] * pow((die_kelv - calib['tref']), 2)
    f_obj_temp = (obj_scale - v_obj_temp) + calib['c2'] * pow((obj_scale - v_obj_temp), 2)
    obj_temp = pow(pow(die_kelv, 4) + (f_obj_temp / s_obj_temp), .25) - 273.15
    return (obj_temp, amb_temp)

def calc_temp_tmp007(obj_temp, amb_temp):
    """
    IR Temperature for Texas Instruments TMP007
    """
    m_scale_lsb = 0.03125
    raw_obj_temp = int(obj_temp >> 2)
    obj_temp_calc = float(raw_obj_temp) * m_scale_lsb
    raw_amb_temp = int(amb_temp >> 2)
    amb_temp_calc = float(raw_amb_temp) * m_scale_lsb
    return (obj_temp_calc, amb_temp_calc)

def calc_hum_sht21(raw_temp, raw_hum):
    """
    Humidity for Sensirion SHT21
    """
    temp_calc = -46.85 + 175.72 / 65536.0 * float(raw_temp)
    raw_hum = float(int(raw_hum) & ~0x0003)
    hum_calc = -6.0 + 125.0 / 65536.0 * float(raw_hum)
    return (temp_calc, hum_calc)

def calc_hum_hdc1000(raw_temp, raw_hum):
    """
    Humidity for Texas Instruments HDC1000
    """
    temp_calc = (float(tosigned(raw_temp)) / 65536.0) * 165.0 - 40.0
    raw_hum = raw_hum & ~0x0003
    hum_calc = (float(raw_hum) / 65536.0) * 100.0
    return (temp_calc, hum_calc)

def calc_accel_kxtj9(raw_x, raw_y, raw_z):
    """
    Accelerometer for Kionix KXTJ9
    """
    accel = lambda v: tosignedbyte(v) / 64.0
    return (accel(raw_x), accel(raw_y), accel(raw_z))

def calc_accel_mpu9250(raw_x, raw_y, raw_z, acc_range=2):
    """
    Accelerometer for InvenSense MPU9250
    """
    accel = lambda v: float(tosigned(v)) / (32768.0 / float(acc_range))
    return (accel(raw_x), accel(raw_y), accel(raw_z))

def calc_gyro_imu3000(raw_x, raw_y, raw_z):
    """
    Gyrometer for InvenSense IMU3000
    """
    gyro = lambda v: float(v) / (65536.0 / 500.0)
    return (gyro(raw_x), gyro(raw_y), gyro(raw_z))

def calc_gyro_mpu9250(raw_x, raw_y, raw_z):
    """
    Gyrometer for InvenSense MPU9250
    """
    gyro = lambda v: float(tosigned(v)) / (65536.0 / 500.0)
    return (gyro(raw_x), gyro(raw_y), gyro(raw_z))

def calc_magnet_mag3110(raw_x, raw_y, raw_z):
    """
    Magnetometer for Freescale MAG3110
    """
    magnet = lambda v: float(tosigned(v)) / (65536.0 / 2000.0)
    return (magnet(raw_x), magnet(raw_y), magnet(raw_z))

def calc_magnet_mpu9250(raw_x, raw_y, raw_z):
    """
    Magnetometer for InvenSense MPU9250
    """
    magnet = lambda v: float(tosigned(v))
    return (magnet(raw_x), magnet(raw_y), magnet(raw_z))

def calc_pressure_bmp280(raw_temp, raw_pressure):
    """
    Barometer for Bosch Sensortec BMP280
    """
    temp_calc = float(raw_temp) / 100.0
    pressure_calc = float(raw_pressure) / 100.0
    return (temp_calc, pressure_calc)

def calc_light_opt3001(raw_lux):
    """
    Luxometer for Texas Instruments OPT3001
    """
    lux_m = raw_lux & ~0x0FFF
    lux_e = (raw_lux & ~0xF000) >> 12
    lux_e = 1 if lux_e == 0 else 2 << (lux_e - 1)
    lux_calc = float(lux_m) * float(lux_e) * 0.01
    return lux_calc


class Barometer(object):
    """
    Barometer for Epcos T5400
    """
    def __init__(self, raw_calib):
        order_bytes = lambda x, y: (x & 0x0FF) + ((y & 0x0FF) << 8)
        self.bar_calib = [order_bytes(x, y) for (x, y) in izip(raw_calib[::2], raw_calib[1::2])]
        return

    def calc(self, raw_temp, raw_pressure):
        """
        Call Temp and Pressure functions
        """
        m_raw_temp = tosigned(raw_temp)
        m_raw_pres = raw_pressure
        bar_temp = self.calc_temp(m_raw_temp)
        bar_pres = self.calc_press(m_raw_temp, m_raw_pres)
        return(bar_temp, bar_pres)

    def calc_temp(self, raw_temp):
        """
        Temperature for Epcos T5400
        """
        val = long((self.bar_calib[0] * raw_temp) * 100)
        temp = val >> 24
        val = long(self.bar_calib[1] * 100)
        temp += (val >> 10)
        return float(temp) / 100.0

    def calc_press(self, raw_temp, raw_pressure):
        """
        Pressure for Epcos T5400
        """
        sensitivity = long(self.bar_calib[2])
        val = long(self.bar_calib[3] * raw_temp)
        sensitivity += (val >> 17)
        val = long(self.bar_calib[4] * pow(raw_temp, 2))
        sensitivity += (val >> 34)
        offset = long(self.bar_calib[5]) << 14
        val = long(self.bar_calib[6] * raw_temp)
        offset += (val >> 3)
        val = long(self.bar_calib[7] * pow(raw_temp, 2))
        offset += (val >> 19)
        pressure = ((sensitivity * raw_pressure) + offset) >> 14
        return float(pressure) / 100.0
