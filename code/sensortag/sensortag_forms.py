from wtforms import Form, BooleanField, SelectField, StringField, \
                    PasswordField, validators, ValidationError

class HexVal(object):
    def __init__(self, *args, **kwargs):
        self.minlength = kwargs.get('minlength', None)
        self.maxlength = kwargs.get('maxlength', None)
        self.minintval = kwargs.get('minintval', None)
        self.maxintval = kwargs.get('maxintval', None)
        self.required = kwargs.get('required', True)
        message = kwargs.get('message', None)
        if not message:
            message = u'Field must be a valid hexadecimal value.'
        self.message = message

    def __call__(self, form, field):
        hexlength = field.data and len(field.data) or 0
        if not hexlength and not self.required:
            print "Skipping validation"
            return
        if (self.minlength and hexlength < self.minlength
                or self.maxlength and hexlength > self.maxlength):
            raise ValidationError(self.message)
        try:
            intval = int(field.data, 16)
        except ValueError:
            raise ValidationError('Field must be a valid hexadecimal value.')
        if (self.minintval and intval < self.minintval
                or self.maxintval and intval > self.maxintval):
            raise ValidationError(self.message)

hexval = HexVal

class LoginForm(Form):
    username = StringField('Username')
    password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])

class SensorTagForm(Form):
    sensorlabel = StringField('Sensor Label')
    sensoraddr = StringField('Sensor Address', [validators.MacAddress()])
    sensorconfig = SelectField(u'Sensor Config', coerce=int)

class SensorConfigForm(Form):
    configlabel = StringField('Config Label')
    tempenabled = BooleanField('IR Temp Enabled')
    tempdataaddr = StringField('IR Temp Data Address', [hexval])
    tempnotifaddr = StringField('IR Temp Notification Address', [hexval])
    tempsensoraddr = StringField('IR Temp Sensor Address', [hexval])
    tempperiodaddr = StringField('IR Temp Period Address', [hexval])
    tempperiod = StringField('IR Temp Period',
            [hexval(minintval=30, maxintval=255, message="Hex value must be between 0x1E and 0xFF")])
    humenabled = BooleanField('Humidity Enabled')
    humdatalength = SelectField(u'Humidity Data Length', choices=[(4, 4), (6, 6)], coerce=int)
    humreqcalib = BooleanField('Humidity Requires Calibration')
    humcalibaddr = StringField('Humidity Calibration Address', [hexval])
    humdataaddr = StringField('Humidity Data Address', [hexval])
    humnotifaddr = StringField('Humidity Notification Address', [hexval])
    humsensoraddr = StringField('Humidity Sensor Address', [hexval])
    humperiodaddr = StringField('Humidity Period Address', [hexval])
    humperiod = StringField('Humidity Collection Period',
            [hexval(minintval=30, maxintval=255, message="Hex value must be between 0x1E and 0xFF")])
