"""
Console sysout handlers for SensorTag data
"""

from zope.interface import implements
from zope.component import adapts, getGlobalSiteManager
from sensortag_callback_interface import \
        IAccelData, IBarometerData, IBatteryData, IGyroData, \
        IHumidityData, ILightData, IMagnetData, ITempData, \
        IAccelHandler, IBarometerHandler, IBatteryHandler, IGyroHandler, \
        IHumidityHandler, ILightHandler, IMagnetHandler, ITempHandler

GSM = getGlobalSiteManager()

class ConsoleAccelHandler(object):
    implements(IAccelHandler)
    adapts(IAccelData)

    def __init__(self, dv):
        self.accel_x = dv.x_value
        self.accel_y = dv.y_value
        self.accel_z = dv.z_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        print '{}: Got Accel X: {}, Y: {}, Z: {}'.format(self.sensor_label,
                                                         self.accel_x,
                                                         self.accel_y,
                                                         self.accel_z)

class ConsoleBarometerHandler(object):
    implements(IBarometerHandler)
    adapts(IBarometerData)

    def __init__(self, dv):
        self.bar_temp = dv.temperature_value
        self.bar_pressure = dv.pressure_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        print '{}: Got Barometer Temperature {}, Pressure {}'.format(self.sensor_label,
                                                                     self.bar_temp,
                                                                     self.bar_pressure)

class ConsoleBatteryHandler(object):
    implements(IBatteryHandler)
    adapts(IBatteryData)

    def __init__(self, dv):
        self.battery_value = dv.battery_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        print '{}: Got Battery Level {}'.format(self.sensor_label, self.battery_value)

class ConsoleGyroHandler(object):
    implements(IGyroHandler)
    adapts(IGyroData)

    def __init__(self, dv):
        self.gyro_x = dv.x_value
        self.gyro_y = dv.y_value
        self.gyro_z = dv.z_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        print '{}: Got Gyro X: {}, Y: {}, Z: {}'.format(self.sensor_label,
                                                        self.gyro_x,
                                                        self.gyro_y,
                                                        self.gyro_z)

class ConsoleHumidityHandler(object):
    implements(IHumidityHandler)
    adapts(IHumidityData)

    def __init__(self, dv):
        self.temperature = dv.temp_value
        self.humidity = dv.humidity_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        print '{}: Got Temperature {}, Humidity {}'.format(self.sensor_label,
                                                           self.temperature,
                                                           self.humidity)

class ConsoleLightHandler(object):
    implements(ILightHandler)
    adapts(ILightData)

    def __init__(self, dv):
        self.lux_value = dv.lux_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        print '{}: Got Light Level {}'.format(self.sensor_label, self.lux_value)

class ConsoleMagnetHandler(object):
    implements(IMagnetHandler)
    adapts(IMagnetData)

    def __init__(self, dv):
        self.mag_x = dv.x_value
        self.mag_y = dv.y_value
        self.mag_z = dv.z_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        print '{}: Got Magnet X: {}, Y: {}, Z: {}'.format(self.sensor_label,
                                                          self.mag_x,
                                                          self.mag_y,
                                                          self.mag_z)

class ConsoleTempHandler(object):
    implements(ITempHandler)
    adapts(ITempData)

    def __init__(self, dv):
        self.target_value = dv.target_value
        self.ambient_value = dv.ambient_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        print '{}: Got Target Temperature {}, Ambient Temperature {}'.format(self.sensor_label,
                                                                             self.target_value,
                                                                             self.ambient_value)

GSM.registerAdapter(ConsoleAccelHandler,
                    (IAccelData,), IAccelHandler, 'print-console')
GSM.registerAdapter(ConsoleBarometerHandler,
                    (IBarometerData,), IBarometerHandler, 'print-console')
GSM.registerAdapter(ConsoleBatteryHandler,
                    (IBatteryData,), IBatteryHandler, 'print-console')
GSM.registerAdapter(ConsoleGyroHandler,
                    (IGyroData,), IGyroHandler, 'print-console')
GSM.registerAdapter(ConsoleHumidityHandler,
                    (IHumidityData,), IHumidityHandler, 'print-console')
GSM.registerAdapter(ConsoleLightHandler,
                    (ILightData,), ILightHandler, 'print-console')
GSM.registerAdapter(ConsoleMagnetHandler,
                    (IMagnetData,), IMagnetHandler, 'print-console')
GSM.registerAdapter(ConsoleTempHandler,
                    (ITempData,), ITempHandler, 'print-console')
