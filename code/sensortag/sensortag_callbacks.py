"""
SensorTag callback functions to be registered as handlers
    in sensortag_listener module
"""

from logging import getLogger
from itertools import izip
from zope.component import getGlobalSiteManager, queryAdapter
from sensortag_callback_interface import \
        AccelData, BarometerData, GyroData, HumidityData, \
        LightData, MagnetData, TempData, \
        IAccelHandler, IBarometerHandler, IGyroHandler, IHumidityHandler, \
        ILightHandler, IMagnetHandler, ITempHandler
import sensortag_callback_logger
import sensortag_callback_console
import sensortag_callback_statsd
from sensortag_calcs import *

GSM = getGlobalSiteManager()
LOG = getLogger()

class SensorCallbacks(object):
    """
    SensorCallbacks class to collect and register all callbacks
    """

    def __init__(self):
        # SensorTag bytes are in a strange endian format:
        # LSB...MSB
        self.order_bytes = lambda x, y: (y << 8) + x
        self.order_triple = lambda x, y, z: (z << 16) + (y << 8) + x

    def handle_tmp007(self, temp_hex, handlers, sensorlabel):
        """
        Calculate object and ambient temperature from raw hex and
            call handlers for TMP007
        """
        obj_temp, amb_temp = \
            tuple(self.order_bytes(x, y) for (x, y) in izip(temp_hex[::2], temp_hex[1::2]))
        m_obj_temp, m_amb_temp = calc_temp_tmp007(obj_temp, amb_temp)
        temperature = TempData(m_obj_temp, m_amb_temp, sensorlabel)
        for handler in handlers:
            adapter = queryAdapter(temperature, ITempHandler, handler)
            if adapter is not None:
                LOG.debug("Attempting temperature callback for handler %s", handler)
                adapter.handle()

    def handle_hdc1000(self, hum_hex, handlers, sensorlabel):
        """
        Calculate humidity and temperature data from raw hex and call handlers for HDC1000
        """
        raw_temp, raw_hum = \
            tuple(self.order_bytes(x, y) for (x, y) in izip(hum_hex[::2], hum_hex[1::2]))
        temp, humidity = calc_hum_hdc1000(raw_temp, raw_hum)
        humidity = HumidityData(temp, humidity, sensorlabel)
        for handler in handlers:
            adapter = queryAdapter(humidity, IHumidityHandler, handler)
            if adapter is not None:
                LOG.debug("Attempting humidity callback for handler %s", handler)
                adapter.handle()

    def handle_mpu9250(self, accel_hex, handlers, sensorlabel):
        """
        Calculate gyro, accel, and magnet data from raw hex and call handlers for MPU9250
        """
        raw_gyro_x, raw_gyro_y, raw_gyro_z, \
            raw_accel_x, raw_accel_y, raw_accel_z, \
            raw_mag_x, raw_mag_y, raw_mag_z = \
            tuple(self.order_bytes(x, y) for (x, y) in izip(accel_hex[::2], accel_hex[1::2]))
        gyro_x, gyro_y, gyro_z = \
            calc_gyro_mpu9250(raw_gyro_x, raw_gyro_y, raw_gyro_z)
        accel_x, accel_y, accel_z = \
            calc_accel_mpu9250(raw_accel_x, raw_accel_y, raw_accel_z)
        mag_x, mag_y, mag_z = \
            calc_magnet_mpu9250(raw_mag_x, raw_mag_y, raw_mag_z)
        gyro_data = GyroData(gyro_x, gyro_y, gyro_z, sensorlabel)
        accel_data = AccelData(accel_x, accel_y, accel_z, sensorlabel)
        mag_data = MagnetData(mag_x, mag_y, mag_z, sensorlabel)
        for handler in handlers:
            gyro_adapter = queryAdapter(gyro_data, IGyroHandler, handler)
            if gyro_adapter is not None:
                LOG.debug("Attempting gyrometer callback for handler %s", handler)
                gyro_adapter.handle()
            accel_adapter = queryAdapter(accel_data, IAccelHandler, handler)
            if accel_adapter is not None:
                LOG.debug("Attempting accelerometer callback for handler %s", handler)
                accel_adapter.handle()
            mag_adapter = queryAdapter(mag_data, IMagnetHandler, handler)
            if mag_adapter is not None:
                LOG.debug("Attempting magnetometer callback for handler %s", handler)
                mag_adapter.handle()

    def handle_bmp280(self, bar_hex, handlers, sensorlabel):
        """
        Calculate temperature and pressure from raw hex and call handlers for BMP280
        """
        raw_temp, raw_pressure = \
            tuple(self.order_triple(x, y, z) for (x, y, z) \
            in izip(bar_hex[::3], bar_hex[1::3], bar_hex[2::3]))
        bar_temp, bar_pressure = \
            calc_pressure_bmp280(raw_temp, raw_pressure)
        bar_data = BarometerData(bar_temp, bar_pressure, sensorlabel)
        for handler in handlers:
            adapter = queryAdapter(bar_data, IBarometerHandler, handler)
            if adapter is not None:
                LOG.debug("Attempting barometer callback for handler %s", handler)
                adapter.handle()

    def handle_opt3001(self, lux_hex, handlers, sensorlabel):
        """
        Calculate lux from raw hex and call handlers for OPT3001
        """
        raw_lux = self.order_bytes(lux_hex[0], lux_hex[1])
        lux = calc_light_opt3001(raw_lux)
        lux_data = LightData(lux, sensorlabel)
        for handler in handlers:
            adapter = queryAdapter(lux_data, ILightHandler, handler)
            if adapter is not None:
                LOG.debug("Attempting luxometer callback for handler %s", handler)
                adapter.handle()
