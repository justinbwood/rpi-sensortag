from zope.interface import implements
from zope.component import adapts, getGlobalSiteManager
from statsd.defaults.env import statsd
from sensortag_callback_interface import \
        IAccelData, IBarometerData, IBatteryData, IGyroData, \
        IHumidityData, ILightData, IMagnetData, ITempData, \
        IAccelHandler, IBarometerHandler, IBatteryHandler, IGyroHandler, \
        IHumidityHandler, ILightHandler, IMagnetHandler, ITempHandler

GSM = getGlobalSiteManager()

class StatsdAccelHandler(object):
    implements(IAccelHandler)
    adapts(IAccelData)

    def __init__(self, dv):
        self.accel_x = dv.x_value
        self.accel_y = dv.y_value
        self.accel_z = dv.z_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        statsd.gauge(self.sensor_label + '.accel.x', self.accel_x)
        statsd.gauge(self.sensor_label + '.accel.y', self.accel_y)
        statsd.gauge(self.sensor_label + '.accel.z', self.accel_z)

class StatsdBarometerHandler(object):
    implements(IBarometerHandler)
    adapts(IBarometerData)

    def __init__(self, dv):
        self.bar_temp = dv.temperature_value
        self.bar_pressure = dv.pressure_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        statsd.gauge(self.sensor_label + '.barometer.temperature', self.bar_temp)
        statsd.gauge(self.sensor_label + '.barometer.pressure', self.bar_pressure)

class StatsdBatteryHandler(object):
    implements(IBatteryHandler)
    adapts(IBatteryData)

    def __init__(self, dv):
        self.battery_value = dv.battery_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        statsd.gauge(self.sensor_label + '.battery', self.battery_value)

class StatsdGyroHandler(object):
    implements(IGyroHandler)
    adapts(IGyroData)

    def __init__(self, dv):
        self.gyro_x = dv.x_value
        self.gyro_y = dv.y_value
        self.gyro_z = dv.z_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        statsd.gauge(self.sensor_label + '.gyro.x', self.gyro_x)
        statsd.gauge(self.sensor_label + '.gyro.y', self.gyro_y)
        statsd.gauge(self.sensor_label + '.gyro.z', self.gyro_z)

class StatsdHumidityHandler(object):
    implements(IHumidityHandler)
    adapts(IHumidityData)

    def __init__(self, dv):
        self.temperature = dv.temp_value
        self.humidity = dv.humidity_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        statsd.gauge(self.sensor_label + '.humidity.temperature', self.temperature)
        statsd.gauge(self.sensor_label + '.humidity.humidity', self.humidity)

class StatsdLightHandler(object):
    implements(ILightHandler)
    adapts(ILightData)

    def __init__(self, dv):
        self.lux_value = dv.lux_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        statsd.gauge(self.sensor_label + '.light', self.lux_value)

class StatsdMagnetHandler(object):
    implements(IMagnetHandler)
    adapts(IMagnetData)

    def __init__(self, dv):
        self.mag_x = dv.x_value
        self.mag_y = dv.y_value
        self.mag_z = dv.z_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        statsd.gauge(self.sensor_label + '.magnet.x', self.mag_x)
        statsd.gauge(self.sensor_label + '.magnet.y', self.mag_y)
        statsd.gauge(self.sensor_label + '.magnet.z', self.mag_z)

class StatsdTempHandler(object):
    implements(ITempHandler)
    adapts(ITempData)

    def __init__(self, dv):
        self.target_value = dv.target_value
        self.ambient_value = dv.ambient_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        statsd.gauge(self.sensor_label + '.temperature.target', self.target_value)
        statsd.gauge(self.sensor_label + '.temperature.ambient', self.ambient_value)

GSM.registerAdapter(StatsdAccelHandler,
                    (IAccelData,), IAccelHandler, 'statsd')
GSM.registerAdapter(StatsdBarometerHandler,
                    (IBarometerData,), IBarometerHandler, 'statsd')
GSM.registerAdapter(StatsdBatteryHandler,
                    (IBatteryData,), IBatteryHandler, 'statsd')
GSM.registerAdapter(StatsdGyroHandler,
                    (IGyroData,), IGyroHandler, 'statsd')
GSM.registerAdapter(StatsdHumidityHandler,
                    (IHumidityData,), IHumidityHandler, 'statsd')
GSM.registerAdapter(StatsdLightHandler,
                    (ILightData,), ILightHandler, 'statsd')
GSM.registerAdapter(StatsdMagnetHandler,
                    (IMagnetData,), IMagnetHandler, 'statsd')
GSM.registerAdapter(StatsdTempHandler,
                    (ITempData,), ITempHandler, 'statsd')
