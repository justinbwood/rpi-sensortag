"""
Module to spawn pexpect and gatttool processes to read in
values from a SensorTag
"""

from logging import getLogger
import threading
import pexpect

LOG = getLogger()

def floatfromhex(hexval):
    """
    Convert hex to signed float
    """
    temp_float = float.fromhex(hexval)
    if temp_float > float.fromhex('7FFF'):
        temp_float = -(float.fromhex('FFFF') - temp_float)
    return temp_float

class SensorTagListener(threading.Thread):
    """
    SensorTag class and methods
    """

    def __init__(self, sensortag, name=None):
        self.bluetooth_addr = sensortag.bt_addr
        self.description = sensortag.label.replace(" ", "")
        if not name:
            name = 'ListenerThread-' + self.description
        LOG.debug("Attempting to connect to SensorTag %s", self.bluetooth_addr)
        self._stopevent = threading.Event()
        threading.Thread.__init__(self, name=name)
        self.con = pexpect.spawn('gatttool -b ' + self.bluetooth_addr + ' --interactive')
        self.con.expect(r'\[LE\]>', timeout=600)
        self.con.sendline('connect')
        self.con.expect(r'Connection successful.*\[LE\]>')
        self.callbacks = {}
        self.handlers = []
        LOG.info("Connected to SensorTag %s", self.bluetooth_addr)
        return

    def write(self, handle, value):
        """
        Write value to SensorTag handle using char-write-cmd
        """
        command = "char-write-cmd 0x{:02x} {:02x}".format(handle, value)
        LOG.debug("Writing Command: %s", command)
        self.con.sendline(command)
        return

    def read(self, handle):
        """
        Read handle from SensorTag using char-read-hnd
        """
        command = "char-read-hnd 0x{:02x}".format(handle)
        LOG.debug("Reading Handle: %s", command)
        self.con.sendline(command)
        self.con.expect('descriptor: .*? \r')
        rval = self.con.after.split()[1:]
        return [long(float.fromhex(n)) for n in rval]

    def run(self):
        """
        Process notification handles in a loop and call
        registered callback handles
        """
        LOG.debug("Starting Thread %s", type(self).__name__)
        while not self._stopevent.isSet():
            try:
                pnum = self.con.expect('Notification handle = .*? \r', timeout=5)
            except pexpect.TIMEOUT:
                LOG.warning('SensorTag %s timed out', self.bluetooth_addr)
                break
            if pnum == 0:
                hex_string = self.con.after.split()[3:]
                handle = long(float.fromhex(hex_string[0]))
                LOG.debug("Calling handler %s on value %s", handle,
                          [long(float.fromhex(n)) for n in hex_string[2:]])
                if handle in self.callbacks:
                    self.callbacks[handle]([long(float.fromhex(n)) for n in hex_string[2:]],
                                           self.handlers, self.description)
                else:
                    LOG.error('Received bad handle: %s', self.con.after)
            else:
                LOG.warning('SensorTag %s timed out', self.bluetooth_addr)

    def join(self, timeout=None):
        self._stopevent.set()
        threading.Thread.join(self, timeout)
        LOG.debug("Stopped Thread %s", type(self).__name__)

    def register_callback(self, handle, callback_function):
        """
        Register function for callback
        """
        self.callbacks[handle] = callback_function
        LOG.debug("Registered Callback: %s, %s", handle, callback_function)
        return

    def register_handlers(self, handlers):
        """
        Register function for handlers
        """
        self.handlers = handlers
        LOG.debug("Registered Handlers: %s", handlers)
