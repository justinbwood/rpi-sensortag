from flask import Blueprint, request, Response
from jsonrpcserver import methods
from sensortag_registry import SensorTagDict
from sensortag_model import SensorTag

RPC = Blueprint('sensortag_rpc', __name__)

APP_STS = SensorTagDict()

@RPC.route('/api/sensor_tag/manage', methods=['POST'])
def manage_sensortag():
    json_data = request.get_json()
    response = methods.dispatch(json_data)
    return Response(str(response), response.http_status,
                    mimetype='application/json')


@methods.add
def enable(id, handlers):
    sensortag = SensorTag.query.get(id)
    if not sensortag:
        return 'not found'
    sensorconfig = sensortag.sensor_config
    APP_STS.enable(sensortag, sensorconfig, handlers)
    return 'success'

@methods.add
def disable(id):
    sensortag = SensorTag.query.get(id)
    if not sensortag:
        return 'not found'
    APP_STS.disable(sensortag)
    return 'success'
