"""
Sensor Tag Flask App

This module creates a Flask app to connect to sensortags, collect
their data, and ship it to statsd

"""

import os
import errno
import logging
from logging.handlers import RotatingFileHandler
from flask import Flask
from flask_cors import CORS
from flask.json import JSONEncoder
from flask_restless_swagger import SwagAPIManager as APIManager
from sqlalchemy_utils import Choice
from sensortag_rpc import RPC
from sensortag_web import WEB
import sensortag_model as model

class CustomJSONEncoder(JSONEncoder):
    """Fix ChoiceType JSON Encoding"""
    def default(self, obj):
        if isinstance(obj, Choice):
            return obj.code

        return JSONEncoder.default(self, obj)

DATABASE_PATH = '/var/lib/sensortag/sensortag.db'

APP = Flask(__name__)
CORS(APP)
APP.config.from_object(__name__)
APP.config.update(dict(
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
APP.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + DATABASE_PATH
APP.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
APP.config.from_envvar('SENSORTAG_SETTINGS', silent=True)

APP.json_encoder = CustomJSONEncoder

APP.register_blueprint(WEB)
APP.register_blueprint(RPC)

HANDLER = RotatingFileHandler('flask.log', maxBytes=10000, backupCount=5)
HANDLER.setLevel(logging.INFO)
LOG = logging.getLogger()
LOG.addHandler(HANDLER)

try:
    os.makedirs(os.path.dirname(DATABASE_PATH))
except OSError as err:
    if err.errno != errno.EEXIST:
        raise

model.DB.app = APP
model.DB.init_app(APP)

API_MANAGER = APIManager(APP, flask_sqlalchemy_db=model.DB)

METHODS_RW = ['GET', 'POST', 'PUT', 'PATCH']
METHODS_ALL = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']

API_MANAGER.create_api(model.SensorTag, methods=METHODS_ALL)
API_MANAGER.create_api(model.SensorConfig, methods=METHODS_ALL)
API_MANAGER.create_api(model.AccelConfig, methods=METHODS_ALL)
API_MANAGER.create_api(model.BarometerConfig, methods=METHODS_ALL)
API_MANAGER.create_api(model.BatteryConfig, methods=METHODS_ALL)
API_MANAGER.create_api(model.GyroConfig, methods=METHODS_ALL)
API_MANAGER.create_api(model.HumidityConfig, methods=METHODS_ALL)
API_MANAGER.create_api(model.LightConfig, methods=METHODS_ALL)
API_MANAGER.create_api(model.MagnetConfig, methods=METHODS_ALL)
API_MANAGER.create_api(model.TempConfig, methods=METHODS_ALL)

@APP.cli.command('initdb')
def initdb_command():
    model.DB.create_all()
    print 'Initialized the database.'
