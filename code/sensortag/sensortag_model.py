from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utils import ChoiceType

DB = SQLAlchemy()

class SensorTag(DB.Model):
    id = DB.Column(DB.Integer, primary_key=True)
    label = DB.Column(DB.String, unique=True, nullable=False)
    bt_addr = DB.Column(DB.String(17), unique=True, nullable=False)
    config = DB.Column(DB.Integer, DB.ForeignKey('sensor_config.id'))

class SensorConfig(DB.Model):
    id = DB.Column(DB.Integer, primary_key=True)
    label = DB.Column(DB.String, unique=True, nullable=False)
    accel_enabled = DB.Column(DB.Boolean(create_constraint=False), nullable=False)
    barometer_enabled = DB.Column(DB.Boolean(create_constraint=False), nullable=False)
    battery_enabled = DB.Column(DB.Boolean(create_constraint=False), nullable=False)
    gyro_enabled = DB.Column(DB.Boolean(create_constraint=False), nullable=False)
    humidity_enabled = DB.Column(DB.Boolean(create_constraint=False), nullable=False)
    light_enabled = DB.Column(DB.Boolean(create_constraint=False), nullable=False)
    magnet_enabled = DB.Column(DB.Boolean(create_constraint=False), nullable=False)
    temp_enabled = DB.Column(DB.Boolean(create_constraint=False), nullable=False)
    sensortags = DB.relationship('SensorTag', backref='sensor_config', lazy=True)
    accel = DB.Column(DB.Integer, DB.ForeignKey('accel_config.id'))
    barometer = DB.Column(DB.Integer, DB.ForeignKey('barometer_config.id'))
    battery = DB.Column(DB.Integer, DB.ForeignKey('battery_config.id'))
    gyro = DB.Column(DB.Integer, DB.ForeignKey('gyro_config.id'))
    humidity = DB.Column(DB.Integer, DB.ForeignKey('humidity_config.id'))
    light = DB.Column(DB.Integer, DB.ForeignKey('light_config.id'))
    magnet = DB.Column(DB.Integer, DB.ForeignKey('magnet_config.id'))
    temp = DB.Column(DB.Integer, DB.ForeignKey('temp_config.id'))

class AccelConfig(DB.Model):
    SENSOR_TYPES = [
        (u'kxtj9', u'Kionix KXTJ9'),
        (u'mpu9250', u'InvenSense MPU9250')
    ]
    id = DB.Column(DB.Integer, primary_key=True)
    label = DB.Column(DB.String, unique=True, nullable=False)
    sensor_type = DB.Column(ChoiceType(SENSOR_TYPES), default=u'mpu9250')
    data_addr = DB.Column(DB.String)
    notif_addr = DB.Column(DB.String)
    sensor_addr = DB.Column(DB.String)
    period_addr = DB.Column(DB.String)
    period = DB.Column(DB.String)
    sensor_config = DB.relationship('SensorConfig', backref='accel_config', lazy=True)

class BarometerConfig(DB.Model):
    SENSOR_TYPES = [
        (u't5400', u'Epcos T5400'),
        (u'bmp280', u'Bosch Sensortec BMP280')
    ]
    id = DB.Column(DB.Integer, primary_key=True)
    label = DB.Column(DB.String, unique=True, nullable=False)
    sensor_type = DB.Column(ChoiceType(SENSOR_TYPES), default=u'bmp280')
    data_addr = DB.Column(DB.String)
    notif_addr = DB.Column(DB.String)
    sensor_addr = DB.Column(DB.String)
    period_addr = DB.Column(DB.String)
    period = DB.Column(DB.String)
    sensor_config = DB.relationship('SensorConfig', backref='barometer_config', lazy=True)

class BatteryConfig(DB.Model):
    id = DB.Column(DB.Integer, primary_key=True)
    label = DB.Column(DB.String, unique=True, nullable=False)
    data_addr = DB.Column(DB.String)
    period = DB.Column(DB.String)
    sensor_config = DB.relationship('SensorConfig', backref='battery_config', lazy=True)

class GyroConfig(DB.Model):
    SENSOR_TYPES = [
        (u'imu3000', u'InvenSense IMU3000'),
        (u'mpu9250', u'InvenSense MPU9250')
    ]
    id = DB.Column(DB.Integer, primary_key=True)
    label = DB.Column(DB.String, unique=True, nullable=False)
    sensor_type = DB.Column(ChoiceType(SENSOR_TYPES), default=u'mpu9250')
    data_addr = DB.Column(DB.String)
    notif_addr = DB.Column(DB.String)
    sensor_addr = DB.Column(DB.String)
    period_addr = DB.Column(DB.String)
    period = DB.Column(DB.String)
    sensor_config = DB.relationship('SensorConfig', backref='gyro_config', lazy=True)

class HumidityConfig(DB.Model):
    SENSOR_TYPES = [
        (u'sht21', u'Sensirion SHT21'),
        (u'hdc1000', u'Texas Instruments HDC1000')
    ]
    id = DB.Column(DB.Integer, primary_key=True)
    label = DB.Column(DB.String, unique=True, nullable=False)
    sensor_type = DB.Column(ChoiceType(SENSOR_TYPES), default=u'hdc1000')
    calib_addr = DB.Column(DB.String)
    data_addr = DB.Column(DB.String)
    notif_addr = DB.Column(DB.String)
    sensor_addr = DB.Column(DB.String)
    period_addr = DB.Column(DB.String)
    period = DB.Column(DB.String)
    sensor_config = DB.relationship('SensorConfig', backref='humidity_config', lazy=True)

class LightConfig(DB.Model):
    SENSOR_TYPES = [
        (u'opt3001', u'Texas Instruments OPT3001')
    ]
    id = DB.Column(DB.Integer, primary_key=True)
    label = DB.Column(DB.String, unique=True, nullable=False)
    sensor_type = DB.Column(ChoiceType(SENSOR_TYPES), default=u'opt3001')
    data_addr = DB.Column(DB.String)
    notif_addr = DB.Column(DB.String)
    sensor_addr = DB.Column(DB.String)
    period_addr = DB.Column(DB.String)
    period = DB.Column(DB.String)
    sensor_config = DB.relationship('SensorConfig', backref='light_config', lazy=True)

class MagnetConfig(DB.Model):
    SENSOR_TYPES = [
        (u'mag3110', u'Freescale MAG3110'),
        (u'mpu9250', u'InvenSense MPU9250')
    ]
    id = DB.Column(DB.Integer, primary_key=True)
    label = DB.Column(DB.String, unique=True, nullable=False)
    sensor_type = DB.Column(ChoiceType(SENSOR_TYPES), default=u'mpu9250')
    data_addr = DB.Column(DB.String)
    notif_addr = DB.Column(DB.String)
    sensor_addr = DB.Column(DB.String)
    period_addr = DB.Column(DB.String)
    period = DB.Column(DB.String)
    sensorconfig = DB.relationship('SensorConfig', backref='magnet_config', lazy=True)

class TempConfig(DB.Model):
    SENSOR_TYPES = [
        (u'tmp006', u'Texas Instruments TMP006'),
        (u'tmp007', u'Texas Instruments TMP007')
    ]
    id = DB.Column(DB.Integer, primary_key=True)
    label = DB.Column(DB.String, unique=True, nullable=False)
    sensor_type = DB.Column(ChoiceType(SENSOR_TYPES), default=u'tmp007')
    data_addr = DB.Column(DB.String)
    notif_addr = DB.Column(DB.String)
    sensor_addr = DB.Column(DB.String)
    period_addr = DB.Column(DB.String)
    period = DB.Column(DB.String)
    sensor_config = DB.relationship('SensorConfig', backref='temp_config', lazy=True)
