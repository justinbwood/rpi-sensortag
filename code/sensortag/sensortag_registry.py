from sensortag_listener import SensorTagListener
from sensortag_callbacks import SensorCallbacks

SENSOR_CALLBACKS = SensorCallbacks()

class SensorTagDict(object):
    def __init__(self):
        self.sensor_dict = {}

    def enable(self, sensortag, sensorconfig, handlers):
        if not sensortag:
            return
        self.disable(sensortag)
        stl = SensorTagListener(sensortag)
        """
        if sensorconfig['tempenabled']:
            stl.write(int(sensorconfig['tempnotifaddr'], 16), 0x01)
            stl.write(int(sensorconfig['tempsensoraddr'], 16), 0x01)
            stl.write(int(sensorconfig['tempperiodaddr'], 16), int(sensorconfig['tempperiod'], 16))
            stl.register_callback(int(sensorconfig['tempdataaddr'], 16), SENSOR_CALLBACKS.handle_tmp007)
        if sensorconfig['humenabled']:
            stl.write(int(sensorconfig['humnotifaddr'], 16), 0x01)
            stl.write(int(sensorconfig['humsensoraddr'], 16), 0x01)
            stl.write(int(sensorconfig['humperiodaddr'], 16), int(sensorconfig['humperiod'], 16))
            stl.register_callback(int(sensorconfig['humdataaddr'], 16), SENSOR_CALLBACKS.handle_hdc1000)
        stl.register_handlers(handlers)
        """
        if sensorconfig.accel_enabled:
            stl.write(int(sensorconfig.accel_config.notif_addr, 16), 0x01)
            stl.write(int(sensorconfig.accel_config.sensor_addr, 16), 0x01)
            stl.write(int(sensorconfig.accel_config.period_addr, 16),
                      int(sensorconfig.accel_config.period, 16))
            if sensorconfig.accel_config.sensor_type == "mpu9250":
                stl.register_callback(int(sensorconfig.accel_config.data_addr, 16),
                                      SENSOR_CALLBACKS.handle_mpu9250)
            # TODO: Implement other callbacks

        if sensorconfig.temp_enabled:
            stl.write(int(sensorconfig.temp_config.notif_addr, 16), 0x01)
            stl.write(int(sensorconfig.temp_config.sensor_addr, 16), 0x01)
            stl.write(int(sensorconfig.temp_config.period_addr, 16),
                      int(sensorconfig.temp_config.period, 16))
            if sensorconfig.temp_config.sensor_type == "tmp007":
                stl.register_callback(int(sensorconfig.temp_config.data_addr, 16),
                                      SENSOR_CALLBACKS.handle_tmp007)

        stl.start()
        self.sensor_dict[sensortag.bt_addr] = stl

    def disable(self, sensortag):
        if not sensortag:
            return
        if sensortag.bt_addr in self.sensor_dict:
            stop_stl = self.sensor_dict[sensortag.bt_addr]
            stop_stl.join()
            del self.sensor_dict[sensortag.bt_addr]
