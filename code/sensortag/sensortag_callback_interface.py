"""
Interfaces for a variety of data that can be returned
by SensorTags. This module contains both data and handler
interfaces, as well as data implementations.
"""

from zope.interface import Interface, Attribute, implements

class IAccelData(Interface):
    """Accelerometer Data Interface"""
    x_value = Attribute("X-Axis Force Value")
    y_value = Attribute("Y-Axis Force Value")
    z_value = Attribute("Z-Axis Force Value")
    sensor_label = Attribute("Sensor Label")

class AccelData(object):
    """Implementation of IAccelData"""
    implements(IAccelData)

    def __init__(self, x_value, y_value, z_value, sensor_label):
        self.x_value = x_value
        self.y_value = y_value
        self.z_value = z_value
        self.sensor_label = sensor_label

class IBarometerData(Interface):
    """Barometer Data Interface"""
    temp_value = Attribute("Barometer Temperature Value")
    pressure_value = Attribute("Barometer Pressure Value")
    sensor_label = Attribute("Sensor Label")

class BarometerData(object):
    """Implementation of IBarometerData"""
    implements(IBarometerData)

    def __init__(self, temp_value, pressure_value, sensor_label):
        self.temp_value = temp_value
        self.pressure_value = pressure_value
        self.sensor_label = sensor_label

class IBatteryData(Interface):
    """Battery Data Interface"""
    battery_value = Attribute("Battery Level Value")
    sensor_label = Attribute("Sensor Label")

class BatteryData(object):
    """Implementation of IBatteryData"""

    def __init__(self, battery_value, sensor_label):
        self.battery_value = battery_value
        self.sensor_label = sensor_label

class IGyroData(Interface):
    """Gyrometer Data Interface"""
    x_value = Attribute("X-Axis Rotation Value")
    y_value = Attribute("Y-Axis Rotation Value")
    z_value = Attribute("Z-Axis Rotation Value")
    sensor_label = Attribute("Sensor Label")

class GyroData(object):
    """Implementation of IGyroData"""
    implements(IGyroData)

    def __init__(self, x_value, y_value, z_value, sensor_label):
        self.x_value = x_value
        self.y_value = y_value
        self.z_value = z_value
        self.sensor_label = sensor_label

class IHumidityData(Interface):
    """Humidity Data Interface"""
    temp_value = Attribute("Temperature Sensor Value")
    humidity_value = Attribute("Humidity Sensor Value")
    sensor_label = Attribute("Sensor Label")

class HumidityData(object):
    """Implementation of IHumidityData"""
    implements(IHumidityData)

    def __init__(self, temp_value, humidity_value, sensor_label):
        self.temp_value = temp_value
        self.humidity_value = humidity_value
        self.sensor_label = sensor_label

class ILightData(Interface):
    """Light Data Interface"""
    lux_value = Attribute("Light Level Value")
    sensor_label = Attribute("Sensor Label")

class LightData(object):
    """Implementation of ILightData"""

    def __init__(self, lux_value, sensor_label):
        self.lux_value = lux_value
        self.sensor_label = sensor_label

class IMagnetData(Interface):
    """Magnetometer Data Interface"""
    x_value = Attribute("X-Axis Magnetic Value")
    y_value = Attribute("Y-Axis Magnetic Value")
    z_value = Attribute("Z-Axis Magnetic Value")

class MagnetData(object):
    """Implementation of IMagnetData"""
    implements(IMagnetData)

    def __init__(self, x_value, y_value, z_value, sensor_label):
        self.x_value = x_value
        self.y_value = y_value
        self.z_value = z_value
        self.sensor_label = sensor_label

class ITempData(Interface):
    """Temp Data Interface"""
    target_value = Attribute("Target Sensor Value")
    ambient_value = Attribute("Ambient Sensor Value")
    sensor_label = Attribute("Sensor Label")

class TempData(object):
    """Implementation of ITempData"""
    implements(ITempData)

    def __init__(self, target_value, ambient_value, sensor_label):
        self.target_value = target_value
        self.ambient_value = ambient_value
        self.sensor_label = sensor_label

class IAccelHandler(Interface):
    """Accelerometer Callback Handler"""
    def handle():
        pass

class IBarometerHandler(Interface):
    """Barometer Callback Handler"""
    def handle():
        pass

class IBatteryHandler(Interface):
    """Battery Callback Handler"""
    def handle():
        pass

class IGyroHandler(Interface):
    """Gyrometer Callback Handler"""
    def handle():
        pass

class IHumidityHandler(Interface):
    """Humidity Callback Handler"""
    def handle():
        pass

class ILightHandler(Interface):
    """Light Callback Handler"""
    def handle():
        pass

class IMagnetHandler(Interface):
    """Magnetometer Callback Handler"""
    def handle():
        pass

class ITempHandler(Interface):
    """Temperature Callback Handler"""
    def handle():
        pass
