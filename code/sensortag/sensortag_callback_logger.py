from logging import getLogger
from zope.interface import implements
from zope.component import adapts, getGlobalSiteManager
from sensortag_callback_interface import \
        IAccelData, IBarometerData, IBatteryData, IGyroData, \
        IHumidityData, ILightData, IMagnetData, ITempData, \
        IAccelHandler, IBarometerHandler, IBatteryHandler, IGyroHandler, \
        IHumidityHandler, ILightHandler, IMagnetHandler, ITempHandler

GSM = getGlobalSiteManager()
LOG = getLogger()

class LoggingAccelHandler(object):
    implements(IAccelHandler)
    adapts(IAccelData)

    def __init__(self, dv):
        self.accel_x = dv.x_value
        self.accel_y = dv.y_value
        self.accel_z = dv.z_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        LOG.info('%s: Got Accel X: %f, Y: %f, Z: %f',
                 self.sensor_label, self.accel_x, self.accel_y, self.accel_z)

class LoggingBarometerHandler(object):
    implements(IBarometerHandler)
    adapts(IBarometerData)

    def __init__(self, dv):
        self.bar_temp = dv.temperature_value
        self.bar_pressure = dv.pressure_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        LOG.info('%s: Got Barometer Temperature %f, Pressure %f',
                 self.sensor_label, self.bar_temp, self.bar_pressure)

class LoggingBatteryHandler(object):
    implements(IBatteryHandler)
    adapts(IBatteryData)

    def __init__(self, dv):
        self.battery_value = dv.battery_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        LOG.info('%s: Got Battery Level %f',
                 self.sensor_label, self.battery_value)

class LoggingGyroHandler(object):
    implements(IGyroHandler)
    adapts(IGyroData)

    def __init__(self, dv):
        self.gyro_x = dv.x_value
        self.gyro_y = dv.y_value
        self.gyro_z = dv.z_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        LOG.info('%s: Got Gyro X: %f, Y: %f, Z: %f',
                 self.sensor_label, self.gyro_x, self.gyro_y, self.gyro_z)

class LoggingHumidityHandler(object):
    implements(IHumidityHandler)
    adapts(IHumidityData)

    def __init__(self, dv):
        self.temperature = dv.temp_value
        self.humidity = dv.humidity_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        LOG.info('%s: Got Temperature %f, Humidity %f',
                 self.sensor_label, self.temperature, self.humidity)

class LoggingLightHandler(object):
    implements(ILightHandler)
    adapts(ILightData)

    def __init__(self, dv):
        self.lux_value = dv.lux_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        LOG.info('%s: Got Light Level %f',
                 self.sensor_label, self.lux_value)

class LoggingMagnetHandler(object):
    implements(IMagnetHandler)
    adapts(IMagnetData)

    def __init__(self, dv):
        self.mag_x = dv.x_value
        self.mag_y = dv.y_value
        self.mag_z = dv.z_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        LOG.info('%s: Got Magnet X: %f, Y: %f, Z: %f',
                 self.sensor_label, self.mag_x, self.mag_y, self.mag_z)

class LoggingTempHandler(object):
    implements(ITempHandler)
    adapts(ITempData)

    def __init__(self, dv):
        self.target_value = dv.target_value
        self.ambient_value = dv.ambient_value
        self.sensor_label = dv.sensor_label

    def handle(self):
        LOG.info('%s: Got Target Temperature %f, Ambient Temperature %f',
                 self.sensor_label, self.target_value, self.ambient_value)

GSM.registerAdapter(LoggingAccelHandler,
                    (IAccelData,), IAccelHandler, 'flask-logging')
GSM.registerAdapter(LoggingBarometerHandler,
                    (IBarometerData,), IBarometerHandler, 'flask-logging')
GSM.registerAdapter(LoggingBatteryHandler,
                    (IBatteryData,), IBatteryHandler, 'flask-logging')
GSM.registerAdapter(LoggingGyroHandler,
                    (IGyroData,), IGyroHandler, 'flask-logging')
GSM.registerAdapter(LoggingHumidityHandler,
                    (IHumidityData,), IHumidityHandler, 'flask-logging')
GSM.registerAdapter(LoggingLightHandler,
                    (ILightData,), ILightHandler, 'flask-logging')
GSM.registerAdapter(LoggingMagnetHandler,
                    (IMagnetData,), IMagnetHandler, 'flask-logging')
GSM.registerAdapter(LoggingTempHandler,
                    (ITempData,), ITempHandler, 'flask-logging')
