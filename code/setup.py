from setuptools import setup

setup(
    name='sensortag',
    packages=['sensortag'],
    include_package_data=True,
    install_requires=[
        'flask',
        'flask_sqlalchemy',
        'pexpect',
        'zope.component',
        'zope.interface',
    ],
)
